#include <stdio.h>
#include <time.h>
#include <stdlib.h>


int main(int argc, char *argv[]){

	if(argc < 2){

		printf("Usage: ./randommind <length of key>\n");

		return -1;

	}
	
	char *length = argv[1];
	
	time_t t;

	srand((unsigned) time(&t));

	int i, size = atoi(length);
	
	char symbols[71] = {'A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '=', '+', '_', ';', ':', '<', '>', '/'};
	
	printf("\n-----------------BEGIN------------------\n\n");

	for(i = 0; i < size; i++)
		
		printf("%c", symbols[rand() % 71]);

	printf("\n\n------------------END-------------------\n\n");

	return 0;
}
